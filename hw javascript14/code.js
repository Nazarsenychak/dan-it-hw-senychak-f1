'use strict'

$(document).ready(function($){
    $('.tab-li').hide();
    $('.tab-li:first').show();
    $('.tabs li:first').addClass('tabs-title-act');
    $('.tabs-title').click(function(){
        $('.tabs-title').removeClass('tabs-title-act').eq($(this).index()).addClass('tabs-title-act');
        $('.tab-li').hide().eq($(this).index()).fadeIn(800);
    });
        
});
    

