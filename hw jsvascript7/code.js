const myList = document.createElement('ol');
document.getElementById('myItemList').appendChild(myList);
const makeList = (array) => {
    array.map((item) => {
        let li = document.createElement('li');
        myList.appendChild(li);
        return li.innerHTML = `${item}`;
    }); 
}
makeList(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv', 'Truskavets']);
