
// global var
const inputBorder = document.querySelector('#input');
const block = document.querySelector('#block');
const btn = document.createElement('button');
const span = document.createElement('span');
const spanIncorrect = document.createElement('span');

// event focus
const inputChange = () => {
    inputBorder.classList.add('input-text-focous');
    let divSpanBtn = document.createElement('div');
       span.innerText = `Поточна ціна: $ `;
    block.insertBefore(divSpanBtn, block.firstChild);
    divSpanBtn.appendChild(span);
    divSpanBtn.appendChild(btn);
    divSpanBtn.classList.add('span-show');
    btn.classList.add('btn-decor');
    btn.innerHTML = 'x';
    
// keyup event
      inputBorder.addEventListener('keyup', () => {
        if (inputBorder.value > 0) {
           span.innerText = `Поточна ціна: $ ` + `${inputBorder.value}`;
           span.classList.add('span-decor');
        }
   }); 
}

const inputChangeBack = () => {
    inputBorder.classList.remove('input-text-focous');
    inputBorder.style.backgroundColor = 'lightgreen';
    inputBorder.style.fontSize = '18px';
    if (inputBorder.value < 0) {
          inputBorder.value = '';
          block.appendChild(spanIncorrect);
          spanIncorrect.innerText = `Please enter correct price`;
          spanIncorrect.classList.add('span-correct');
          inputBorder.style.backgroundColor = 'red';
    } else if (inputBorder.value > 0) {
          inputBorder.value = '';
          spanIncorrect.remove();
    }
    span.value = '';

    btn.addEventListener('click', function() {
      span.remove();
      span.value = '';
      inputBorder.value = '';
      btn.remove();
      spanIncorrect.remove();
      inputBorder.style.backgroundColor = 'white';
    });
}

inputBorder.addEventListener('focus', inputChange);
inputBorder.addEventListener('blur', inputChangeBack);

