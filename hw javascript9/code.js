window.onload = function () {
    document.querySelector('.tabs').addEventListener('click', fTabs);

    function fTabs(event) {    
        if (event.target.className == 'tabs-title') {
            let dataTab = event.target.getAttribute('data-tab');
            let tabH = document.getElementsByClassName('tabs-title');
            // remove active
              for (let i = 0; i < tabH.length; i++) {
                  tabH[i].classList.remove('tabs-title-act');
              }
            event.target.classList.add('tabs-title-act');
            let tabBodyArr = document.getElementsByClassName('tab-li');
            // loop for show & none
              for (let i = 0; i < tabBodyArr.length; i++) {
                  if (dataTab == i) {
                      tabBodyArr[i].style.display = 'block';
                  } else {
                    tabBodyArr[i].style.display = 'none';
                  }
              } 
        }
        
    }

}