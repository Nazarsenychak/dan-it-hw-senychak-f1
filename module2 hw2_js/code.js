
class Hamburger {
    constructor(size, stuffing) {
        try {
            if (size === undefined || stuffing === undefined) {
                throw new HamburgerException ('no size or stuffing')
            } 
            if (size.type !== 'size') {
                throw new HamburgerException ('invalid size or stuffing')
            }
            if (stuffing.type !== 'stuff') {
                throw new HamburgerException ('invalid size or stuffing')
            }
        this._size = size
        this._stuffing = stuffing
        this._toppings = []
    } catch (e) {
        console.log(e.name + e.message)
    }
  }

   addTopping(topping) {  
        try {
            if (topping === undefined){
                throw new HamburgerException('Havent topping!')
            } 
             if(topping.type !== 'topp') {
                throw new HamburgerException('Is not Topping')
            }
             if (this._toppings.includes(topping)){
                throw new HamburgerException('One Topping to get twice')
            }
             this._toppings.push(topping);
        }catch (e) {
            console.log(e.name + e.message);
        }
    }

    removeTopping(topping) {
        try {
            if (topping === undefined){
                throw new HamburgerException('Havent topping!') 
            }
             return this._toppings = this._toppings.filter(e => e !== topping)
      } catch(e) {
        console.log(e.name + e.message);
      }
    }

    get getToppings() {
        return this._toppings;
    }

    get getSize() {
        if (this._size === Hamburger.SIZE_LARGE) {
          return this._size.name 
        } else {
            return this._size.name 
        }
    }

    get getStuffing() {
        return this._stuffing;
    }

    get calculatePrice() {
       // get array toppings and plus all prices
       const arrayToppings = this._toppings;
       let sumPrice = 0 
       arrayToppings.forEach(i => sumPrice += i.price)
       this.price = sumPrice + this._size.price + this._stuffing.price;
       return this.price;
 }

    get calculateCalories() {
        // get array toppings and plus all ccal
        const arrayToppings = this._toppings;
        let ccal = 0 
        arrayToppings.forEach(i => ccal += i.ccal)
        this.ccal = ccal + this._size.ccal + this._stuffing.ccal;
        return this.ccal;
    }
}

Hamburger.SIZE_SMALL = {
    price: 50,
    ccal: 20,
    type: 'size',
    name: 'small',
}
Hamburger.SIZE_LARGE = {
    price: 100,
    ccal: 40,
    type: 'size',
    name: 'large',
}
Hamburger.STUFFING_CHEESE = {
    price: 10,
    ccal: 20,
    type: 'stuff',
}
Hamburger.STUFFING_SALAD = {
    price: 20,
    ccal: 5,
    type: 'stuff',
}
Hamburger.STUFFING_POTATO = {
    price: 15,
    ccal: 10,
    type: 'stuff',
}
Hamburger.TOPPING_MAYO = {
    price: 20,
    ccal: 5,
    type: 'topp',
    name: 'mayo',
}
Hamburger.TOPPING_SPICE = {
    price: 15,
    ccal: 0,
    type: 'topp',
    name: 'spice',
}

// to show message error
function HamburgerException(message) {
    this.message = message
    this.name = 'HamburgerException: '
}

const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE)



hamburger.addTopping(Hamburger.TOPPING_MAYO)
hamburger.addTopping(Hamburger.TOPPING_SPICE)
hamburger.removeTopping(Hamburger.TOPPING_MAYO)


console.log("Price: %f", hamburger.calculatePrice)
console.log("Calories: %f", hamburger.calculateCalories)
console.log("Have %d toppings", hamburger.getToppings.length)
console.log("Is hamburger large?: %s", hamburger.getSize)
