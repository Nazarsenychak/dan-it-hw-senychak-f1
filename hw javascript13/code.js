
 function  ready () {
    const container = document.getElementById('container-doc');
        if (localStorage.getItem('bgcolor') !== null) {
            let color = localStorage.getItem('bgcolor');
            container.style.backgroundColor = color;
        }
    document.querySelector('.tabs').addEventListener('click', fTabs);
    function fTabs(event) {    
        if (event.target.className == 'tabs-title') {
            let dataTab = event.target.getAttribute('data-tab');
            let tabH = document.getElementsByClassName('tabs-title');
            // remove active
              for (let i = 0; i < tabH.length; i++) {
                  tabH[i].classList.remove('tabs-title-act');
              }
            event.target.classList.add('tabs-title-act');
            let tabBodyArr = document.getElementsByClassName('tab-li');
            // loop for show & none
              for (let i = 0; i < tabBodyArr.length; i++) {
                  if (dataTab == i) {
                      tabBodyArr[i].style.display = 'block';
                  } else {
                    tabBodyArr[i].style.display = 'none';
                  }
              } 
        }        
    }
   
    document.getElementById('green').onclick = function () {
        container.style.backgroundColor = 'rgba(66,212,80,0.8)';
        localStorage.setItem('bgcolor', 'rgba(66,212,80,0.8)');
    }
   document.getElementById('dark').onclick = function () {
        container.style.backgroundColor = 'rgba(16,13,19,0.8)';
        localStorage.setItem('bgcolor', 'rgba(16,13,19,0.8)');
    }
    document.getElementById('blue').onclick = function () {
        container.style.backgroundColor = 'rgba(0,96,187,0.8)';
        localStorage.setItem('bgcolor', 'rgba(0,96,187,0.8)');
    }
    document.getElementById('without').onclick = function () {
        container.style.backgroundColor = 'rgba(255,255,255,1)';
        localStorage.setItem('bgcolor', 'rgba(255,255,255,1)');
    }
}
    document.addEventListener("DOMContentLoaded", ready);