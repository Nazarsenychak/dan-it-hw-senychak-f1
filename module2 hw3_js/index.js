
// create table
let body = document.querySelector('body')
createTable(body, 30, 30)

function createTable(parent, cols, rows) {
    let table = document.createElement('table')
    for (let i = 0; i < rows; i++) {
        let tr = document.createElement('tr')
        
        for (let j = 0; j < cols; j++) {
            let td = document.createElement('td')
            tr.appendChild(td)
            td.setAttribute('id', 'td')
        }
        table.appendChild(tr)
    }
    parent.appendChild(table)
    
// add event on table
$(table).on('click', function(e) {
    toggleColor(e.target)
})
// event on body
$(body).on('click', function(e) {
    if (e.target === body) {
        let tab = document.querySelectorAll('td')
        toggleColorBack(tab)
    }
})
// func to change bg td
function toggleColor (event) {
    $(event).toggleClass('black')
}
// func change rest td on another color 
function toggleColorBack (tab) {
    tab.forEach(element => {
     if(element.classList.contains('black')) {
            element.classList.remove('black')
        } else{
          element.classList.add('black')
     }
    })
  }
}