'use strict'
function SliderMake (classContainer) {
    // take elements
    let images = document.querySelectorAll(classContainer);
    const stop = document.getElementById('stop');
    const play = document.getElementById('play');
// loop for img
    images.forEach((items) => {
        items.style.display = 'none';
        items.style.width = '40%';
    });
    let currentPicture = 0;
    // show 1 img
    images[currentPicture].style.display = 'block';

    // to clear interval and hide btn play
    const clearCarouselInterval = () => {
        clearInterval(this.carouselInterval);
        this.carouselInterval = setInterval(changeSlide, 10000);
        play.style.display = 'none';
    };
// central function who change img
    const changeSlide = () => {
        images[currentPicture].style.display = 'none';
        currentPicture = (currentPicture + 1) % images.length;
        images[currentPicture].style.display = 'block';
    };
// call changeSlide
    this.carouselInterval = function () {
        setInterval(changeSlide, 10000);
    }
// hide btn play
    play.style.display = 'none';
    // click for btn stop
    stop.addEventListener('click', () => {
        clearInterval(this.carouselInterval);
        play.style.display = 'block';
    });
    // click for btn play
    play.addEventListener('click', clearCarouselInterval);
}
// the construct function
const constructirSlide = new SliderMake('.image-to-show').carouselInterval();    
















// // take img & btn
// const images = document.querySelectorAll('.image-to-show');
// let btnStop = document.getElementById('stop');
// // loop for array of elements
// images.forEach(item => {
//     item.style.display = 'none';
// });
// let currentSlide = 0;
// images[currentSlide].style.display = 'block';
// // function for clear interval and hide btn start
// const clearCaruselInterval = () => {
//     clearInterval(caruselInt);
//     caruselInt = setInterval(changeImg, 1000);
//     start.style.display = 'none';
// }
// // central funtion
// const changeImg = () => {
//     images[currentSlide].style.display = 'none';
//     currentSlide = (currentSlide + 1) % images.length;
//     images[currentSlide].style.display = 'block';
// };
// // call setInterval
// let caruselInt = setInterval(changeImg, 1000);

// let start = document.getElementById('start');
// start.style.display = 'none';
// // event for btn stop
// btnStop.addEventListener('click', (e) => {
//     console.log(e.target);
//     clearInterval(caruselInt);
//     start.style.display = 'block';
//     start.addEventListener('click', clearCaruselInterval);
// });