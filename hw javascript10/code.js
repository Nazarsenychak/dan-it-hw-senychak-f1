let iconChangeOne = document.getElementById('icon-one');
let iconChangeTwo = document.getElementById('icon-two');
let pasFirst = document.getElementById('pas-first');
let pasSecond = document.getElementById('pas-second');
// valid function
function handleIcon(icon, input){
  icon.addEventListener('click', function(){
    let arrClasses = icon.classList;
    if(arrClasses.contains('fa-eye')){
      arrClasses.toggle('fa-eye');
      arrClasses.toggle('fa-eye-slash');
      if(input.type === 'password'){
          input.setAttribute('type','text');
      }
    } else if(arrClasses.contains('fa-eye-slash')){
      arrClasses.toggle('fa-eye-slash');
      arrClasses.toggle('fa-eye');
      if(input.type === 'text'){
          input.setAttribute('type','password');
      }
    }
  });

}
// check two password
function handlePass(inputFirst, inputSecond){
  let span = document.createElement('span');
  document.getElementById('btn-valid').addEventListener('click', function(){
    if (inputFirst.value === inputSecond.value && inputFirst.value !== '' && inputSecond.value !== '') {
        span.remove();
        alert('You are welcome!');
    } else {
        span.style.color = 'red';
        span.style.margin = '10px';
        span.style.padding = '10px';
        span.style.border = '1px solid red';
        span.style.borderRadius = '8px';
        span.innerText = 'Введіть одинакові значення!';
        document.getElementById('error').append(span);  
    }
  }); 
}

handleIcon(iconChangeOne, pasFirst);
handleIcon(iconChangeTwo, pasSecond);
handlePass(pasFirst, pasSecond);