function Hamburger(size, stuffing) { 
    try {
        if (size === undefined || stuffing === undefined) {
            throw new HamburgerException ('no size or stuffing')
        } 
        if (size.type !== 'size') {
            throw new HamburgerException ('invalid size or stuffing')
        }
        if (stuffing.type !== 'stuff') {
            throw new HamburgerException ('invalid size or stuffing')
        }
        this.size = size;
        this.stuffing = stuffing;
        this.toppings = [];
    } catch (e) {
        console.log(e.name + e.message)
    }
} 


Hamburger.SIZE_SMALL = {
    price: 50,
    ccal: 20,
    type: 'size',
    name: 'small',
}
Hamburger.SIZE_LARGE = {
    price: 100,
    ccal: 40,
    type: 'size',
    name: 'large',
}
Hamburger.STUFFING_CHEESE = {
    price: 10,
    ccal: 20,
    type: 'stuff',
}
Hamburger.STUFFING_SALAD = {
    price: 20,
    ccal: 5,
    type: 'stuff',
}
Hamburger.STUFFING_POTATO = {
    price: 15,
    ccal: 10,
    type: 'stuff',
}
Hamburger.TOPPING_MAYO = {
    price: 20,
    ccal: 5,
    type: 'topp',
    name: 'mayo',
}
Hamburger.TOPPING_SPICE = {
    price: 15,
    ccal: 0,
    type: 'topp',
    name: 'spice',
}


Hamburger.prototype.addTopping = function (topping)  {
    try {
        if (topping === undefined){
            throw new HamburgerException('Havent topping!')
        }
         if(topping.type !== 'topp') {
            throw new HamburgerException('Is not Topping')
        }
         if (this.toppings.includes(topping)){
            throw new HamburgerException('One Topping to get twice')
        }
         this.toppings.push(topping);
    }catch (e) {
        console.log(e.name + e.message);
    }
}

Hamburger.prototype.removeTopping = function (topping) {
    try {
        if (topping === undefined){
            throw new HamburgerException('Havent topping!') 
        }
         return this.toppings = this.toppings.filter(e => e !== topping)
  } catch(e) {
    console.log(e.name + e.message);
  }
}

Hamburger.prototype.getToppings = function () {
    return this.toppings;
}

Hamburger.prototype.getSize = function () {
    if (this.size === Hamburger.SIZE_LARGE) {
        return this.size.name 
      } else {
        return this.size.name  
      }
}

Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
}

Hamburger.prototype.calculatePrice = function () {
    // get array toppings and plus all prices
    const arrayToppings = this.toppings;
    let sumPrice = 0 
    arrayToppings.forEach(i => sumPrice += i.price)
    this.price = sumPrice + this.size.price + this.stuffing.price;
    return this.price;
}

Hamburger.prototype.calculateCalories = function () {
    // get array toppings and plus all ccalories
    const arrayToppings = this.toppings;
    let ccal = 0 
    arrayToppings.forEach(i => ccal += i.ccal)
    this.ccal = ccal + this.size.ccal + this.stuffing.ccal;
    return this.ccal;
}

// to show message error
function HamburgerException(message) {
    this.message = message
    this.name = 'HamburgerException: '
}

// to create construction
let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_POTATO);

// explore class
hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.removeTopping(Hamburger.TOPPING_SPICE);


console.log("Price: %f", hamburger.calculatePrice());
console.log("Calories: %f", hamburger.calculateCalories());
console.log("Have %d toppings", hamburger.getToppings().length);
console.log("Is hamburger large: %s", hamburger.getSize());

